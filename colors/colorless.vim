" vim:sw=8:ts=8
"

hi clear
if exists("syntax_on")
    syntax reset
endif

let colors_name = "colorless"

if !has('gui_running')
  hi Normal       cterm=none          ctermbg=none    ctermfg=none
  hi SpecialKey   cterm=BOLD          ctermbg=none    ctermfg=none
  hi IncSearch    cterm=REVERSE       ctermbg=none    ctermfg=none
  hi Search       cterm=REVERSE       ctermbg=none    ctermfg=none
  hi MoreMsg      cterm=BOLD          ctermbg=none    ctermfg=none
  hi ModeMsg      cterm=BOLD          ctermbg=none    ctermfg=none
  hi CursorLineNr cterm=italic        ctermbg=none    ctermfg=none
  hi LineNr       cterm=none          ctermbg=none    ctermfg=none
  hi StatusLine   cterm=BOLD,REVERSE  ctermbg=none    ctermfg=none
  hi StatusLineNC cterm=REVERSE       ctermbg=none    ctermfg=none
  hi VertSplit    cterm=REVERSE       ctermbg=none    ctermfg=none
  hi Title        cterm=BOLD          ctermbg=none    ctermfg=none
  hi Visual       cterm=REVERSE       ctermbg=none    ctermfg=none
  hi VisualNOS    cterm=BOLD          ctermbg=none    ctermfg=none
  hi WarningMsg   cterm=STANDOUT      ctermbg=none    ctermfg=none
  hi WildMenu     cterm=STANDOUT      ctermbg=none    ctermfg=none
  hi Folded       cterm=STANDOUT      ctermbg=none    ctermfg=none
  hi FoldColumn   cterm=STANDOUT      ctermbg=none    ctermfg=none
  hi DiffAdd      cterm=BOLD          ctermbg=none    ctermfg=none
  hi DiffChange   cterm=BOLD          ctermbg=none    ctermfg=none
  hi DiffDelete   cterm=BOLD          ctermbg=none    ctermfg=none
  hi DiffText     cterm=REVERSE       ctermbg=none    ctermfg=none
  hi Type         cterm=none          ctermbg=none    ctermfg=none
  hi Keyword      cterm=none          ctermbg=none    ctermfg=none
  hi Number       cterm=none          ctermbg=none    ctermfg=none
  hi Char         cterm=none          ctermbg=none    ctermfg=none
  hi Format       cterm=none          ctermbg=none    ctermfg=none
  hi Special      cterm=none          ctermbg=none    ctermfg=none
  hi Constant     cterm=none          ctermbg=none    ctermfg=none
  hi PreProc      cterm=none          ctermbg=none    ctermfg=none
  hi Directive    cterm=none          ctermbg=none    ctermfg=none
  hi Conditional  cterm=none          ctermbg=none    ctermfg=none
  hi Comment      cterm=BOLD          ctermbg=none    ctermfg=none
  hi Func         cterm=none          ctermbg=none    ctermfg=none
  hi Identifier   cterm=none          ctermbg=none    ctermfg=none
  hi Statement    cterm=none          ctermbg=none    ctermfg=none
  hi Ignore       cterm=BOLD          ctermbg=none    ctermfg=none
  hi String       term=UNDERLINE      ctermbg=none    ctermfg=none
  hi ErrorMsg     cterm=REVERSE       ctermbg=none    ctermfg=none
  hi Error        cterm=REVERSE       ctermbg=none    ctermfg=none
  hi Todo         cterm=BOLD,STANDOUT ctermbg=none    ctermfg=none
  hi MatchParen   cterm=BOLD,REVERSE  ctermbg=none    ctermfg=none
  hi ColorColumn  cterm=REVERSE       ctermbg=none    ctermfg=none
else
  hi Normal       gui=none          guibg=none    guifg=none
  hi SpecialKey   gui=BOLD          guibg=none    guifg=none
  hi IncSearch    gui=REVERSE       guibg=none    guifg=none
  hi Search       gui=REVERSE       guibg=none    guifg=none
  hi MoreMsg      gui=BOLD          guibg=none    guifg=none
  hi ModeMsg      gui=BOLD          guibg=none    guifg=none
  hi CursorLineNr gui=italic        guibg=none    guifg=none
  hi LineNr       gui=none          guibg=none    guifg=none
  hi StatusLine   gui=BOLD,REVERSE  guibg=none    guifg=none
  hi StatusLineNC gui=REVERSE       guibg=none    guifg=none
  hi VertSplit    gui=REVERSE       guibg=none    guifg=none
  hi Title        gui=BOLD          guibg=none    guifg=none
  hi Visual       gui=REVERSE       guibg=none    guifg=none
  hi VisualNOS    gui=BOLD          guibg=none    guifg=none
  hi WarningMsg   gui=STANDOUT      guibg=none    guifg=none
  hi WildMenu     gui=STANDOUT      guibg=none    guifg=none
  hi Folded       gui=STANDOUT      guibg=none    guifg=none
  hi FoldColumn   gui=STANDOUT      guibg=none    guifg=none
  hi DiffAdd      gui=BOLD          guibg=none    guifg=none
  hi DiffChange   gui=BOLD          guibg=none    guifg=none
  hi DiffDelete   gui=BOLD          guibg=none    guifg=none
  hi DiffText     gui=REVERSE       guibg=none    guifg=none
  hi Type         gui=none          guibg=none    guifg=none
  hi Keyword      gui=none          guibg=none    guifg=none
  hi Number       gui=none          guibg=none    guifg=none
  hi Char         gui=none          guibg=none    guifg=none
  hi Format       gui=none          guibg=none    guifg=none
  hi Special      gui=none          guibg=none    guifg=none
  hi Constant     gui=none          guibg=none    guifg=none
  hi PreProc      gui=none          guibg=none    guifg=none
  hi Directive    gui=none          guibg=none    guifg=none
  hi Conditional  gui=none          guibg=none    guifg=none
  hi Comment      gui=BOLD          guibg=none    guifg=none
  hi Func         gui=none          guibg=none    guifg=none
  hi Identifier   gui=none          guibg=none    guifg=none
  hi Statement    gui=none          guibg=none    guifg=none
  hi Ignore       gui=BOLD          guibg=none    guifg=none
  hi String       gui=UNDERLINE     guibg=none    guifg=none
  hi ErrorMsg     gui=REVERSE       guibg=none    guifg=none
  hi Error        gui=REVERSE       guibg=none    guifg=none
  hi Todo         gui=BOLD,STANDOUT guibg=none    guifg=none
  hi MatchParen   gui=BOLD,REVERSE  guibg=none    guifg=none
  hi ColorColumn  gui=REVERSE       guibg=none    guifg=none
endif
